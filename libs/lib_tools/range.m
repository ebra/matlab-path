function r=range(x)
    r=[min(x(:)) max(x(:))];
end
